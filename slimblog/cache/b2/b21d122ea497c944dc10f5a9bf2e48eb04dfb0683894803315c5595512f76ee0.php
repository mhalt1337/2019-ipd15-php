<?php

/* register.html.twig */
class __TwigTemplate_f7e257a1a833bf21677b6177ffa85ab29287b030c5e1db1c8d9cc8c9d1fb458e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Register";
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "
    ";
        // line 19
        if (($context["errorList"] ?? null)) {
            echo "    
        <ul class=\"error\">
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 22
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "        </ul>
    ";
        }
        // line 26
        echo "
    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", array()), "html", null, true);
        echo "\" >
        <span id=\"emailTaken\" class=\"error\"></span><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>    
        <input type=\"submit\" value=\"Register\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 28,  80 => 26,  76 => 24,  67 => 22,  63 => 21,  58 => 19,  55 => 18,  52 => 17,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{%extends \"master.html.twig\"%}

{% block title %}Register{% endblock %}

{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
{% endblock %}

{% block content %}

    {% if errorList %}    
        <ul class=\"error\">
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\" >
        <span id=\"emailTaken\" class=\"error\"></span><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>    
        <input type=\"submit\" value=\"Register\">
    </form>

{% endblock %}
", "register.html.twig", "C:\\xampp\\htdocs\\2019-ipd15-php\\slimblog\\templates\\register.html.twig");
    }
}
