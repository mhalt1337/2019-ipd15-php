<?php

require_once 'vendor/autoload.php';

DB::$user = 'first';
DB::$dbName = 'first';
DB::$password = 'IVbGHXOEHPqO5GTl';
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = 'utf8';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});

$app->get('/hello/:name/:age', function ($name, $age) use ($app) {
    // echo "Hello, $name, you are $age y/o";
    DB::insert('people', array(
        'name' => $name,
        'age' => $age
    ));
    $app->render('hello.html.twig', array(
        'name' => $name,
        'age' => $age
    ));
});

$app->get('/list', function() {
    $peopleList = DB::query("SELECT * FROM people");
    print_r($peopleList);
});

$app->get('/addme', function() use ($app) {
    // state 1: first show
    $app->render('addme.html.twig');
});

$app->post('/addme', function() use ($app) {
    // receiving submission
    $name = $app->request()->post('name');
    $age = $app->request()->post('age');
    // verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name must be 2-100 characters long");
    }
    if (!is_numeric($age) || $age < 1 || $age > 150) {
        array_push($errorList, "Age must be an integer in 1-150 range");
    }
    //
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('people', array(
            'name' => $name,
            'age' => $age
        ));
        $app->render('addme_success.html.twig');
    } else {
        // state 3: failed submission
        $app->render('addme.html.twig', array('errorList' => $errorList));
    }
});


$app->get('/random', function() use ($app) {
    // state 1: first show
    $app->render('random.html.twig');
});

$app->post('/random', function() use ($app) {
    // receiving submission
    $min = $app->request()->post('min');
    $max = $app->request()->post('max');
    $count = $app->request()->post('count');
    $valueList = array('min' => $min, 'max' => $max, 'count' => $count);
    // verify submission
    $errorList = array();
    if (!is_numeric($min)) {
        array_push($errorList, "Min count must be provided and numerical");
        unset($valueList['min']);
    }
    if (!is_numeric($max)) {
        array_push($errorList, "Max count must be provided and numerical");
        unset($valueList['max']);
    }
    if (!is_numeric($count)) {
        array_push($errorList, "Count count must be provided and numerical");
        unset($valueList['count']);
    }
    if ($min > $max) {
        array_push($errorList, "Min must be smaller or equal to maximum");
        unset($valueList['min']);
        unset($valueList['max']);
    }
    if ($count < 1) {
        array_push($errorList, "Count must 1 or greater");
        unset($valueList['count']);
    }
    //
    if (!$errorList) {
        // state 2: successful submission
        $randList = array();
        for ($i=0; $i<$count; $i++) {
            array_push($randList, rand($min, $max));
        }
        $app->render('random.html.twig', array(
            'v' => $valueList,
            'randList' => $randList
                ));
    } else {
        // state 3: failed submission
        $app->render('random.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
                ));
    }
});

$app->run();
