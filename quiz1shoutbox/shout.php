<?php
require_once 'db.php';
?><!DOCTYPE html>
<html>
    <head>
        <link href="styles.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>Shout</title>
    </head>
    <body>
        <div id="centeredContent">
            <?php

            function getForm($name = "", $message = "") {
                $form = <<< MARKER
<form method="post">
    Name: <input type="text" name="name" value="$name"><br>
    Message: <input type="text" name="message" value="$message"><br>
    <input type="submit" value="Shout">
</form>
                        <br>
MARKER;
                return $form;
            }

            if (isset($_POST['name'])) { // State 2 or 3 - receiving submission
                $name = $_POST['name'];
                $message = $_POST['message'];
                $errorList = array();
                //
                if (strlen($name) < 2 || strlen($name) > 20) {
                    array_push($errorList, "name must be 2-20 characters long");
                } else {
                    if (preg_match('/^[A-Za-z0-9\s_]+$/', $name) != 1) {
                        array_push($errorList, "Username must be composed of upper/lower case letters and numbers");
                    }
                }

                if (strlen($message) < 1 || strlen($message) > 100) {
                    array_push($errorList, "message must be 1-100 characters long");
                }

                if ($errorList) { // state 3: errors
                    echo "<h3>Problems detected</h3>";
                    echo "<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li>" . $error . "</li>\n";
                    }
                    echo "</ul>\n";

                    $query = "SELECT * FROM shouts ORDER BY ts DESC LIMIT 10";
                    $result = mysqli_query($link, $query);
                    if (!$result) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }

                    echo getForm($name, $message);
                    $query = "SELECT * FROM shouts ORDER BY ts DESC LIMIT 10";
                    $result = mysqli_query($link, $query);
                    if (!$result) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }
                    echo "<div id=shoutsList>\n";
                    while ($row = mysqli_fetch_assoc($result)) {
                        $id = $row['id'];
                        $ts = $row['ts'];
                        $authorName = $row['name'];
                        $shout = $row['message'];
                        printf("<div class=shoutHead>* On %s <a href=\"user.php?name=%s\">%s</a> shouted : %s<br></div>\n", $ts, $authorName, $authorName, $shout);
                    }
                    echo "</div>\n";

                    echo "<br>YOU HAVE SHOUTED _" . $_SESSION['count'] . "_ TIMES FROM THIS BROWSER SESSION";
                } else { // state 2: submission successful
                    // insert record into users table
                    $insert = sprintf("INSERT INTO shouts VALUES (NULL, '%s', '%s', NULL)", mysqli_real_escape_string($link, $name), mysqli_real_escape_string($link, $message));
                    $insertResult = mysqli_query($link, $insert);
                    if (!$insertResult) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }


                    echo getForm();
                    $query = "SELECT * FROM shouts ORDER BY ts DESC LIMIT 10";
                    $result = mysqli_query($link, $query);
                    if (!$result) {
                        echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                        exit;
                    }
                    echo "<div id=shoutsList>\n";
                    while ($row = mysqli_fetch_assoc($result)) {
                        $id = $row['id'];
                        $ts = $row['ts'];
                        $authorName = $row['name'];
                        $shout = $row['message'];
                        printf("<div class=shoutHead>* On %s <a href=\"user.php?name=%s\">%s</a> shouted : %s<br></div>\n", $ts, $authorName, $authorName, $shout);
                    }
                    echo "</div>\n";

                    if (!isset($_SESSION['count'])) {
                        $_SESSION['count'] = 0;
                    }
                    $_SESSION['count'] ++;

                    echo "<br>YOU HAVE SHOUTED _" . $_SESSION['count'] . "_ TIMES FROM THIS BROWSER SESSION";
                }
            } else { // state 1: first show
                echo getForm();
            }
            ?>
        </div>
    </body>
</html>
