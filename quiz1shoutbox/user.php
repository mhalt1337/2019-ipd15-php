<?php
require_once 'db.php';
?><!DOCTYPE html>
<html>
    <head>
        <link href="styles.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>Shout</title>       
    </head>
    <body>
        <div id="centeredContent">
            <?php
            if (!isset($_GET['name'])) {
                echo "<h3>Name is missing</h3>\n";
                echo "<p><a href=shout.php>Go back to shout</a></p>";
                exit;
            }
            $name = $_GET['name'];
            $query = "SELECT * FROM shouts WHERE name='$name'";
            $result = mysqli_query($link, $query);
            if (!$result) {
                echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                exit;
            }
                    echo "<ul>\n";
            if ($row = mysqli_fetch_assoc($result)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $id = $row['id'];
                    $ts = $row['ts'];
                    $authorName = $row['name'];
                    $shout = $row['message'];
                    printf("<li>On %s <a href=\"user.php?name=%s\">%s</a> shouted : %s</li>", $ts, $authorName, $authorName, $shout);
                }
                    echo "</ul>\n";
            } else {
                echo "<p>No Records Found!</p>\n";
            }
            ?>
        </div>
    </body>
</html>
