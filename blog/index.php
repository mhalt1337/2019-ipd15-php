<?php
session_start();
require_once 'db.php';
?><!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link href="styles.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>Index</title>
    </head>
    <body>
        <div id="centeredContent">                       
        <?php
        if (isset($_SESSION['user'])) {
            $username = $_SESSION['user']['username'];
            echo "<p>You are logged int as $username. "
                    . "You can <a href=\"articleadd.php\">post an article</a>"
                    . " or <a href=\"logout.php\">logout</a>.</p>";
        } else {
            echo "<p><a href=\"login.php\">login</a> or <a href=\"register.php\"> register</a>"
            . " to post articles and comments</p>";
        }
        //
        $query = "SELECT a.id, a.creationTime, a.title, a.body, u.username authorName " .
                " FROM articles as a, users as u WHERE a.authorId = u.id";
        $result = mysqli_query($link, $query);
        if (!$result) {
            echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
            exit;
        }
        
        echo "<div id=articlesList>\n";
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row['id'];
            $creationTime = $row['creationTime'];
            $title = $row['title'];
            $body = $row['body'];
            $authorName = $row['authorName'];
            // print_r($row); echo "<br>\n";
            printf("<div class=artHead><a href=\"article.php?id=%s\"><b>%s</b></a><br>\nPosted by %s on %s<br>\n%s</div>",
                    $id, $title, $authorName, $creationTime, $body);
        }
        echo "</div>\n";
        ?>
        </div>
    </body>
</html>
