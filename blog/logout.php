<?php
require_once 'db.php';
session_start();
?><!DOCTYPE html>
<html>
    <head>
        <link href="styles.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>Logout</title>       
    </head>
    <body>
        <div id="centeredContent">
            <?php
                // avoid using session_destroy(); it removes ALL session variables
                if (isset($_SESSION['user'])) {
                    unset($_SESSION['user']);
                }
                echo "<p>You've been logged out. <a href=index.php>click to continue</a>.</p>\n";
            ?>
        </div>
    </body>
</html>
