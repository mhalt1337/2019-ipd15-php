<?php
require_once 'db.php';
session_start();
?><!DOCTYPE html>
<html>
    <head>
        <link href="styles.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>Article</title>       
    </head>
    <body>
        <div id="centeredContent">
<?php
    if (!isset($_GET['id'])) {
        echo "<h3>Article Id missing</h3>\n";
        echo "<p><a href=index.php>Go back to index</a></p>";
        exit;
    }
    $articleId = $_GET['id'];
    $query = "SELECT a.id, a.creationTime, a.title, a.body, u.username authorName " .
                " FROM articles as a, users as u WHERE a.authorId = u.id AND a.id=" 
           . mysqli_real_escape_string($link, $articleId);
        $result = mysqli_query($link, $query);
        if (!$result) {
            echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
            exit;
        }
        if ($row = mysqli_fetch_assoc($result)) {
            $id = $row['id'];
            $creationTime = $row['creationTime'];
            $title = $row['title'];
            $body = $row['body'];
            $authorName = $row['authorName'];
            // print_r($row); echo "<br>\n";
            printf("<div class=artHead><b>%s</b><br>\nPosted by %s on %s<br></div>\n%s",
                    $title, $authorName, $creationTime, $body);
        } else {
            echo "<p>Error: article not found</p>\n";
        }
?>
        </div>
    </body>
</html>
