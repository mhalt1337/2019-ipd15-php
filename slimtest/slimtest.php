<?php

session_start();
require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'slimtest';
DB::$dbName = 'slimtest';
DB::$password = 'g1xwaLXUW6GKoBmD';
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = 'utf8';
DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array(
    'id'=>'\d+'
));

$app->get('/products/add', function() use ($app, $log) {
    $app->render('product_add.html.twig');
});

$app->get('/products/:id', function($id) use ($app, $log) {
    $product = DB::queryFirstRow("SELECT * FROM products WHERE id=%i", $id);
    if (!$product) {
        $app->notFound();
        return;
    }
    $app->render('product_view.html.twig', array('p' => $product));
});


$app->post('/products/add', function() use ($app, $log) {
    $name = $app->request()->post('name');
    $desc = $app->request()->post('desc');
    $price = $app->request()->post('price');
    $valueList = array('name' => $name, 'desc' => $desc, 'price' => $price);

    $errorList = array();
    if (strlen($name) < 1 || strlen($name) > 100) {
        array_push($errorList, "Name must be 1-100 characters long");
    }
    if (strlen($desc) < 1 || strlen($desc) > 2000) {
        array_push($errorList, "Description must be 1-2000 characters long");
    }

    if (!is_numeric($price) || $price < 0 || $price > 999999.99) {
        array_push($errorList, "Price must be 0-99999.99");
    }

    $image = $_FILES['image'];
    $imageInfo = getimagesize($image['tmp_name']);

    if (!$imageInfo) {
        array_push($errorList, "File does not look like a valid image");
    } else {
        // never allow .. in file names because it can change the directory
        if (strstr($image['name'], "..")) {
            array_push($errorList, "File name invalid");
        }
        //only allow specific extensions
        $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
        if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
            array_push($errorList, "File extension invalid");
        }
        //DO NOT ALLOW OVERRIDE AN EXISTING FILE
        if (file_exists('uploads/' . $image['name'])) {
            array_push($errorList, "File name already exists");
        }
    }
    //
    if ($errorList) {
        $app->render('product_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
        ));
    } else {
        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        DB::insert('products', array(
            'name' => $name,
            'description' => $desc,
            'price' => $price,
            'imagePath' => $image['name']
        ));
        $productId = DB::insertId();
        $app->render('product_add_success.html.twig', array('productId' => $productId));
    }
});

$app->run();
