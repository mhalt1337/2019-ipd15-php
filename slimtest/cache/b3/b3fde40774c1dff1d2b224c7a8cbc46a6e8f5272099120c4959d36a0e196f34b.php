<?php

/* master.html.twig */
class __TwigTemplate_4af65d0d1ace238c8af7f5a52ea00ee689db0387988a7eb2638699586e12b2c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"../styles.css\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " - Slim Test</title>
        ";
        // line 6
        $this->displayBlock('addhead', $context, $blocks);
        // line 8
        echo "    </head>
    <body>
        <div id=\"centeredContent\">";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        // line 11
        echo "            <div id=\"footer\">
                &copy; Copyright 2011 by <a href=\"http://localhost:8001/\">you</a>.
            </div>
        </div>
    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 6
    public function block_addhead($context, array $blocks = array())
    {
        echo "        
        ";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  61 => 10,  54 => 6,  49 => 5,  40 => 11,  38 => 10,  34 => 8,  32 => 6,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"../styles.css\" />
        <title>{% block title %}{% endblock %} - Slim Test</title>
        {% block addhead %}        
        {% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">{% block content %}{% endblock %}
            <div id=\"footer\">
                &copy; Copyright 2011 by <a href=\"http://localhost:8001/\">you</a>.
            </div>
        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\2019-ipd15-php\\slimtest\\templates\\master.html.twig");
    }
}
