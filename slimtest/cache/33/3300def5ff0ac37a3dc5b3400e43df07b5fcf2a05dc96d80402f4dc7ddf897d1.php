<?php

/* product_view.html.twig */
class __TwigTemplate_40e9e6ee04b6f2332d57a31098112fff9a1bdb9b1bbef30382c38b94fe9366f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " - Product";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <div class=\"productView\">
        <h3>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "name", array()), "html", null, true);
        echo " for \$";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "price", array()), "html", null, true);
        echo "</h3>
        <div>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "description", array()), "html", null, true);
        echo "</div>
        <img src=\"/uploads/";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "imagePath", array()), "html", null, true);
        echo "\" width=\"200\">
    </div>
";
    }

    public function getTemplateName()
    {
        return "product_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 9,  47 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{%extends \"master.html.twig\"%}

{% block title %} - Product{% endblock %}

{% block content %}
    <div class=\"productView\">
        <h3>{{p.name}} for \${{p.price}}</h3>
        <div>{{p.description}}</div>
        <img src=\"/uploads/{{p.imagePath}}\" width=\"200\">
    </div>
{% endblock %}
", "product_view.html.twig", "C:\\xampp\\htdocs\\2019-ipd15-php\\slimtest\\templates\\product_view.html.twig");
    }
}
