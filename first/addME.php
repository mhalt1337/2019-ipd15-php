<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        function displayForm() {
            ?><form>
                Name: <input name="name"><br>
                Age: <input name="age" type="number"><br>
                <input type="submit" value="Say Hello!">
            </form> <?php
        }

        if (isset($_GET['name'])) {
            $name = $_GET['name'];
            $age = $_GET['age'];

            //is the submission valid?
            if ((strlen($name) > 2) && ($age > 0 && $age < 150)) {
                echo "<p>Hello $name, you are $age years old</p>";
            } else {
                echo "<p>ERROR: Name must be greater than 2 and age " .
                "must be between 1 - 149</p>";
                displayForm();
            }
        } else {
            displayForm();
        }
        ?>


    </body>
</html>
