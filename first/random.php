
<?php

// heredoc
$form = <<< MARKER
<form>
    Min: <input type="number" name="min"><br>
    Max: <input type="number" name="max"><br>
    How many to generate? <input type="number" name="count"><br>
    <input type="submit" value="Generate">
</form>
MARKER;

if (isset($_GET['min'])) { // State 2 or 3 - receiving submission
    $min = $_GET['min'];
    $max = $_GET['max'];
    $count = $_GET['count'];
    $errorList = array();
    //
    if (!is_numeric($min) || !is_numeric($max) || !is_numeric($count)) {
        array_push($errorList, "Min, max, and count must be numbers");
    } else {
        if ($min > $max) {
            array_push($errorList, "Min must be less or equal to max");
        }
        if ($count < 1) {
            array_push($errorList, "Count must be 1 or greater");
        }
    }
    //
    if ($errorList) { // state 3: errors
        // echo "State 3: errors\n";
        // print_r($errorList);
        echo "<h3>Problems detected</h3>";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>".$error."</li>\n";
        }
        echo "</ul>\n";
        echo $form;
    } else { // state 2: submission successful
        echo "<p>Random numbers: ";
        for ($i=0; $i<$count; $i++) {
            echo $i==0 ? "" : ", ";
            echo rand($min, $max);
        }
        echo "</p>\n";
        echo "<p><a href=random.php>Go back and randomize again</a></p>\n";
    }
    
} else { // state 1: first show
    echo $form;
}


